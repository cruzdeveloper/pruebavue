import { db, firebase, FS_COLLECTIONS } from "@/firebase";
import router from "@/router";

export default {
  async register({ commit, dispatch, state }, datos) {
    commit("SET_ERROR", '');
    try {
      await firebase
        .auth()
        .createUserWithEmailAndPassword(datos.email, datos.password);
    } catch (error) {
      switch (error.code) {
        case "auth/email-already-in-use":
          commit("SET_ERROR", "correo en uso");
          break;
      }
      return;
    }
    const curretUser = firebase.auth().currentUser;
    datos.uid = curretUser.uid;
    datos.fecha_registro = new Date();
    datos.activo = true;
    await db.collection("usuarios").doc(curretUser.uid).set(datos);
    await dispatch("cargarUsuario", curretUser.uid);
    commit("comprobarLogin", true);
  },
  async cargarUsuario({ commit }, uid) {
    try {
      localStorage.removeItem("usuario");
      let docRef = db.collection("usuarios").doc(uid);
      let user = await docRef.get();
      if (user.exists) {
        //console.log("existe");
        let usuario = user.data();
        commit("SET_USER", usuario);
        localStorage.setItem("paciente", JSON.stringify(usuario));
      }
    } catch (error) {
      console.log(error);
    }
  },
  async verificarSesion({ commit, dispatch }) {
    try {
      firebase.auth().onAuthStateChanged(() => {
        const user = firebase.auth().currentUser;
        //console.log("authchanged",user)
        if (user) {
          commit("comprobarLogin", true);
          let usuario = JSON.parse(localStorage.getItem("usuario"));
          if (usuario) {
            commit("SET_USER", usuario);
          }
        } else {
          commit("comprobarLogin", null);
        }
      });
    } catch (error) {
      console.log(error);
    }
  },

  async logginOut({ commit, dispatch }) {
    try {
      await firebase.auth().signOut();
      localStorage.removeItem("usuario");
      commit("SET_USER", null);
      commit("comprobarLogin", null);
      return;
    } catch (error) {
      console.log(error);
    }
  },

  async ingresarUsuario({ dispatch, state, commit }, datos) {
    commit("SET_ERROR", '');
    const { email, password} = datos;
    let user;
    await firebase
      .auth()
      .signInWithEmailAndPassword(email, password)
      .then(async (userCredential) => {
        // Signed in
        user = userCredential.user;
        console.log(user);
        await dispatch("cargarUsuario", user.uid);
        commit("SET_ERROR", '');
      })
      .catch((error) => {
        var errorCode = error.code;
        var errorMessage = error.message;
        console.log(errorCode);
        switch (errorCode) {
          case "auth/invalid-email":
            commit("SET_ERROR", "Correo invalido");
            break;
          case "auth/wrong-password":
            commit("SET_ERROR", "Contraseña o correo incorrecto");
            break;
          case "auth/user-not-found":
            commit("SET_ERROR", "Usuario no encontrado");
            break;
        }
      });
  },
};
