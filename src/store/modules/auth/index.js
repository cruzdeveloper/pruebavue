import state from './state'
import actions from './actions'
import getters from './getters'
import mutations from './mutations'

const authModule = {
    namespaced:true,
    state,
    getters,
    actions,
    mutations,
}

export default authModule