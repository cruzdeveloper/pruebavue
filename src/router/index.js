import Vue from 'vue'
import VueRouter from 'vue-router'
import {createRouter} from 'vue-router'
import Login from '@/views/Login.vue'
import {firebase} from "@/firebase";
Vue.use(VueRouter)

const routes = [
  {
    // Document title tag
    // We combine it with defaultDocumentTitle set in `src/main.js` on router.afterEach hook
    meta: {
      title: 'Login',
      requiresAuth: false,
    },
    path: '/',
    name: 'login',
    component: Login
  },
  {
    meta: {
      title: 'Tables',
      requiresAuth: true,
    },
    path: '/tables',
    name: 'tables',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "tables" */ '@/views/Tables.vue')
  },
  {
    meta: {
      title: 'Forms',
      requiresAuth: true,
    },
    path: '/forms',
    name: 'forms',
    component: () => import(/* webpackChunkName: "forms" */ '@/views/Forms.vue')
  },
  {
    meta: {
      title: 'Profile',
      requiresAuth: true,
    },
    path: '/profile',
    name: 'profile',
    component: () => import(/* webpackChunkName: "profile" */ '@/views/Profile.vue')
  },
  {
    meta: {
      title: 'New Client',
      requiresAuth: true,
    },
    path: '/client/new',
    name: 'client.new',
    component: () => import(/* webpackChunkName: "client-form" */ '@/views/ClientForm.vue')
  },
  {
    meta: {
      title: 'Edit Client',
      requiresAuth: true,
    },
    path: '/client/:id',
    name: 'client.edit',
    component: () => import(/* webpackChunkName: "client-form" */ '@/views/ClientForm.vue'),
    props: true
  },
  {
    meta: {
      title: 'Registro',
      requiresAuth: false,
    },
    path: '/register',
    name: 'register',
    component: () => import(/* webpackChunkName: "client-form" */ '@/views/Register.vue'),
    props: true
  },
]

const router = new VueRouter({
  routes,
  scrollBehavior (to, from, savedPosition) {
    if (savedPosition) {
      return savedPosition
    } else {
      return { x: 0, y: 0 }
    }
  }
})

router.beforeEach((to, from, next) => {
  
  const requiresAuth = to.matched.some(x => x.meta.requiresAuth)
  const isLogin = to.name==='login'
  console.log(to.name)

  if(requiresAuth) {
    firebase.auth().onAuthStateChanged( (user) => {
      
      if (!user) return next('/')
      if(isLogin) return next('/forms')

      next()

    })
  } else {

    if(isLogin){
      firebase.auth().onAuthStateChanged( (user) => {
        if (!user) return next()
        return next('/forms')
  
      })

    } else {
      next()
    }

  }
})

export default router

export function useRouter () {
  return router
}
