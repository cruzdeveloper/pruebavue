import firebase from 'firebase/compat';
import 'firebase/compat/auth';
import 'firebase/compat/firestore';
import 'firebase/compat/storage';
import { FIREBASE_CONFIG } from './environment';


firebase.initializeApp(FIREBASE_CONFIG)


const db  = firebase.firestore()
const storage = firebase.storage()
export {
  firebase,
  db,
  storage
}
